import { shallowMount } from '@vue/test-utils'
import Home from '@/pages/index.vue'
import Bar from '@/components/Bar.vue'
import Button from '@/components/Button.vue'

let wrapper

beforeEach(() => {
  wrapper = shallowMount(Home)
})

describe('Home', () => {
  test('is a Vue instance', () => {
    expect(wrapper.isVueInstance()).toBeTruthy()
  })

  test('render bars, options and buttons', async () => {
    await wrapper.setData({
      data: {
        bars: [1, 2, 3],
        buttons: [1, 2, 3, 4],
        limit: 100
      }
    })
    expect(wrapper.findAll(Bar).length).toBe(wrapper.vm.data.bars.length)
    expect(wrapper.findAll(Button).length).toBe(wrapper.vm.data.buttons.length)
    expect(wrapper.findAll('select option').length).toBe(
      wrapper.vm.data.bars.length
    )
  })
})
