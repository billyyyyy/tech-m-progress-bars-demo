import { shallowMount } from '@vue/test-utils'
import Button from '@/components/Button.vue'

describe('Button', () => {
  test('is a Vue instance', () => {
    const wrapper = shallowMount(Button)
    expect(wrapper.isVueInstance()).toBeTruthy()
  })

  test('display value as text', () => {
    const wrapper = shallowMount(Button, {
      propsData: {
        value: 123
      }
    })
    expect(wrapper.text()).toContain(123)
  })

  test('button has correct background color', async () => {
    const wrapper = shallowMount(Button, {
      propsData: {
        value: 123
      }
    })
    expect(wrapper.classes('bg-blue-100')).toBe(true)
    await wrapper.setProps({ value: -123 })
    expect(wrapper.classes('bg-red-100')).toBe(true)
  })

  test('button click', () => {
    const wrapper = shallowMount(Button)
    const mockFn = jest.fn()
    wrapper.setMethods({
      onClick: mockFn
    })
    wrapper.trigger('click')
    expect(mockFn).toBeCalled()
  })
})
