import { shallowMount } from '@vue/test-utils'
import Bar from '@/components/Bar.vue'

let wrapper

beforeEach(() => {
  wrapper = shallowMount(Bar, {
    propsData: { value: 10, limit: 100 }
  })
})

describe('Button', () => {
  test('is a Vue instance', () => {
    expect(wrapper.isVueInstance()).toBeTruthy()
  })

  test('label', async () => {
    expect(wrapper.text()).toBe('10%')
    await wrapper.setProps({ value: 120 })
    expect(wrapper.text()).toBe('120%')
  })

  test('width', async () => {
    expect(wrapper.vm.width).toBe('10')
    await wrapper.setProps({ value: 120 })
    expect(wrapper.vm.width).toBe('100')
    await wrapper.setProps({ value: 0 })
    expect(wrapper.vm.width).toBe('0')
  })

  test('color', async () => {
    expect(wrapper.vm.color).toBe('bg-blue-400')
    await wrapper.setProps({ value: 120 })
    expect(wrapper.vm.color).toBe('bg-red-400')
  })
})
